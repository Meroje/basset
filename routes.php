<?php use Basset\Basset as Basset;
return array(

	'GET /basset/landing.css' => function()
	{
		return Basset::add('style', 'landing.css');
	},

	'GET /basset/landing.js' => function()
	{
		return Basset::add('jquery', 'libs/jquery-1.6.3.min.js')
			->add('tipsy', 'jquery.tipsy.js');
	},

	'GET /basset/template.css' => function()
	{
		return Basset::add('fluid-960', '960.gs.fluid.css')
			->add('simple-lists', 'simple-lists.css')
			->add('block-lists', 'block-lists.css')
			->add('planning', 'planning.css')
			->add('table', 'table.css')
			->add('calendars', 'calendars.css')
			->add('wizard', 'wizard.css')
			->add('gallery', 'gallery.css');
	},

	'GET /basset/global.css' => function()
	{
		return Basset::add('reset', 'reset.css')
			->add('common', 'common.css')
			->add('form', 'form.css')
			->add('standard', 'standard.css')
			->add('special-pages', 'special-pages.css');
	},

	'GET /basset/wizard.css' => function()
	{
		return Basset::add('wizard', 'wizard.css');
	},

	'GET /basset/modernizr.js' => function()
	{
		return Basset::add('modernizr', 'libs/modernizr.custom.min.js');
	},

	'GET /basset/generic.js' => function()
	{
		return Basset::add('jquery', 'libs/jquery-1.6.3.min.js')
			->add('old-browsers', 'old-browsers.js'); // remove if you do not need older browsers detection
	},

	'GET /basset/template-common.js' => function ()
	{
		return Basset::add('common', 'common.js')
			->add('standard', 'standard.js');
	},

	'GET /basset/template.js' => function()
	{
		return Basset::add('jquery.accessibleList', 'jquery.accessibleList.js')
			->add('searchField', 'searchField.js')
			->add('jquery.tip', 'jquery.tip.js')
			->add('jquery.contextMenu', 'jquery.contextMenu.js')
			->add('jquery.modal', 'jquery.modal.js');
	},

	'GET /basset/template.ie.js' => function()
	{
		return Basset::add('standard.ie', 'standard.ie.js');
	},

	'GET /basset/custom-styles.js' => function()
	{
		return Basset::add('list', 'list.js');
	},

	'GET /basset/plugins.js' => function()
	{
		return Basset::add('jquery.dataTables', 'libs/jquery.dataTables.min.js')
			->add('jquery.datepick', 'libs/jquery.datepick/jquery.datepick.min.js')
			->add('jquery.hashchange', 'libs/jquery.hashchange.js');
	},

	'GET /basset/charts.js' => function()
	{
		return Basset::add('charts', 'http://www.google.com/jsapi');
	},

	'GET /basset/template-setup.js' => function()
	{
		return Basset::add('template-setup', 'template-setup.js');
	},
);